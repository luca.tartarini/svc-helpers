#!/usr/bin/env bash

#===============================================================================
# Title        : aws-config.sh
# Description  : This script returns a set of temporary credentials for an IAM 
#                user with MFA.
# Author       : Luca Tartarini
# Date         : 15/03/2021
# Version      : 0.1
# Usage:       : . ./aws-config -u=<name.surname> -t=<mfa_token>
#===============================================================================

unset AWS_ACCESS_KEY_ID
unset AWS_SECRET_ACCESS_KEY
unset AWS_SESSION_TOKEN
unset AWS_PROFILE
unset AWS_REGION

unset USERNAME
unset TOKEN

REGION="eu-west-1"

help() {
   echo "Returns a set of temporary credentials for an IAM user with MFA."
   echo
   echo "Syntax: aws-config.sh -u=[username] -t=[token]"
   echo
   echo "Mandatory options:"
   echo "-u | --username    IAM username."
   echo "-t | --token       MFA token code."
   echo
}

main() {
  for i in "$@"; do
    case $i in
    -u=* | --username=*)
      USERNAME="${i#*=}"
      shift # past argument=value
      ;;
    -t=* | --token=*)
      TOKEN="${i#*=}"
      shift # past argument=value
      ;;
    -h | --help)
      help
      return
      ;;
    *)
      echo "Unknown option: ${i}, exiting..."
      return
      ;;
    esac
  done

  if [ -z ${USERNAME+x} ] && [ -z ${TOKEN+y} ]; then
    echo "USERNAME and TOKEN are unset."
    return
  elif [ -z ${USERNAME+x} ]; then
    echo "USERNAME is unset."
    return
  elif [ -z ${TOKEN+y} ]; then
    echo "TOKEN is unset."
    return
  fi

  if [[ ${#TOKEN} != 6 ]]; then
    echo "Malformed MFA TOKEN."
    return
  fi

  echo "USERNAME   = ${USERNAME}"
  echo "MFA TOKEN  = ${TOKEN}"

  export AWS_PROFILE="${USERNAME}"
  export AWS_REGION=eu-west-1

  RESPONSE=$(aws sts get-session-token --serial-number arn:aws:iam::537084507879:mfa/"${USERNAME}" --region "${AWS_REGION}" \
    --token-code "${TOKEN}")

  if [ "$(echo "${RESPONSE}" | jq -r ".Credentials.SessionToken")" ]
  then
    AWS_ACCESS_KEY_ID=$(echo "${RESPONSE}" | jq -r ".Credentials.AccessKeyId")
    AWS_SECRET_ACCESS_KEY=$(echo "${RESPONSE}" | jq -r ".Credentials.SecretAccessKey")
    AWS_SESSION_TOKEN=$(echo "${RESPONSE}" | jq -r ".Credentials.SessionToken")
  else
    echo "Error in getting session token, exiting..."
    return
  fi

  export AWS_ACCESS_KEY_ID
  export AWS_SECRET_ACCESS_KEY
  export AWS_SESSION_TOKEN
}

main "$@"
