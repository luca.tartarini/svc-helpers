#!/usr/bin/env bash

#===============================================================================
# Title        : marketing-aws-config.sh
# Description  : This script returns a set of temporary credentials for an IAM 
#                user with MFA.
# Author       : Luca Tartarini
# Date         : 15/03/2021
# Version      : 0.1
# Usage:       : . ./marketing-aws-config -u=<name.surname> -t=<mfa_token>
#===============================================================================

unset AWS_ACCESS_KEY_ID
unset AWS_SECRET_ACCESS_KEY
unset AWS_SESSION_TOKEN
unset AWS_PROFILE

unset USERNAME
unset TOKEN

help() {
   echo "Returns a set of temporary credentials for an IAM user with MFA."
   echo
   echo "Syntax: marketing-aws-eks-config.sh -u=[username] -t=[token]"
   echo
   echo "Mandatory options:"
   echo "-u | --username    IAM username."
   echo "-t | --token       MFA token code."
   echo
}

main() {
  for i in "$@"
  do
    case $i in
      -u=*|--username=*)
      USERNAME="${i#*=}"
      shift # past argument=value
      ;;
      -t=*|--token=*)
      TOKEN="${i#*=}"
      shift # past argument=value
      ;;
      -h|--help)
      help
      return;;
      *)
      echo "Unknown option: ${i}, exiting..."
      return;;
    esac
  done

  if [ -z ${USERNAME+x} ] && [ -z ${TOKEN+y} ]; then 
    echo "USERNAME and TOKEN are unset."
    return
  elif [ -z ${USERNAME+x} ]; then
    echo "USERNAME is unset."
    return
  elif [ -z ${TOKEN+y} ]; then
    echo "TOKEN is unset."
    return 
  fi

  if [[ ${#TOKEN} != 6 ]]; then
    echo "Malformed MFA TOKEN."
    return
  fi

  echo "USERNAME   = ${USERNAME}"
  echo "MFA TOKEN  = ${TOKEN}"

  export AWS_PROFILE="${USERNAME}"
  export AWS_REGION=eu-west-1

  response=$(aws sts get-session-token --serial-number arn:aws:iam::067676556723:mfa/"${USERNAME}" \
    --token-code "${TOKEN}" --profile marketing)

  if [ "$(echo "${response}" | jq -r ".Credentials.SessionToken")" ]
  then
    AWS_ACCESS_KEY_ID=$(echo "${response}" | jq -r ".Credentials.AccessKeyId")
    AWS_SECRET_ACCESS_KEY=$(echo "${response}" | jq -r ".Credentials.SecretAccessKey")
    AWS_SESSION_TOKEN=$(echo "${response}" | jq -r ".Credentials.SessionToken")
  else
    echo "Error in getting session token, exiting..."
    return
  fi

  export AWS_ACCESS_KEY_ID
  export AWS_SECRET_ACCESS_KEY
  export AWS_SESSION_TOKEN
}

main "$@"
