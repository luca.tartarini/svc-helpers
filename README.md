# Service Helpers

## aws-config
Bash script to login via IAM user and MFA token to AWS.

```
source ./aws-config.sh --username=<name.surname> --token=<mfa_token> 
```

You can either create a symbolic link for the scripts to run it as a command:
```
ln -s /<your-path-to-this-repo>/aws-config.sh /usr/local/bin/aws-config
```

## aws-eks-config
Bash script to login via IAM user and MFA token to AWS and authenticate against an EKS cluster.

```
source ./aws-eks-config.sh --username=<name.surname> --role=<role> --token=<mfa_token>
```

You can either create a symbolic link for the scripts to run it as a command:
```
ln -s /<your-path-to-this-repo>/eks-config.sh /usr/local/bin/eks-config
```

## DevOps Box
### Build the Docker image
```
docker build --no-cache -t devopsbox:latest -f dockerfiles/Dockerfile .
```

### Testing the Docker image
```
docker run --rm devopsbox aws --version
docker run --rm devopsbox terraform --version
docker run --rm devopsbox ansible --version
docker run --rm devopsbox packer --version
```

### Run the Docker container
If you need to interact with AWS, be sure to have correctly logged in using the `aws-config` tool.

Usage example (`aws s3 ls` command):
```
docker run --rm \
    -w /opt \
    -v $(pwd):/opt/ \
    -v ~/.aws:/root/.aws \
    -v ~/.ssh:/root/.ssh \
    -e AWS_REGION=$AWS_REGION \
    -e AWS_PROFILE=$AWS_PROFILE \
    -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
    -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
    -e AWS_SESSION_TOKEN=$AWS_SESSION_TOKEN \
    devopsbox aws s3 ls
```
### CLI Commands
To create commands with your current directory mapped as working directory in the docker image, add the following to your profile (.bashrc or .bash_profile).

```
dansible () 
{
  docker run --rm \
             -w /opt \
             -v $(pwd):/opt/ \
             -v ~/.aws:/root/.aws \
             -v ~/.ssh:/root/.ssh \
             devopsbox ansible $@
}

daws () 
{
  docker run --rm \
             -w /opt \
             -v $(pwd):/opt/ \
             -v ~/.aws:/root/.aws \
             -v ~/.ssh:/root/.ssh \
             devopsbox aws $@
}

dpacker () 
{
  docker run --rm \
             -w /opt \
             -v $(pwd):/opt/ \
             -v ~/.aws:/root/.aws \
             -v ~/.ssh:/root/.ssh \
             devopsbox packer $@
}

dterraform () 
{
  docker run --rm \
             -w /opt \
             -v $(pwd):/opt/ \
             -v ~/.aws:/root/.aws \
             -v ~/.ssh:/root/.ssh \
             devopsbox terraform $@
}
```
