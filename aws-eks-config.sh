#!/usr/bin/env bash

#===============================================================================
# Title        : aws-eks-config.sh
# Description  : This script returns a set of temporary credentials for an IAM
#                user with MFA and configures kubectl so that you can connect to
#                an Amazon EKS cluster.
# Author       : Luca Tartarini
# Date         : 15/03/2021
# Version      : 0.1
# Usage:       : . ./aws-eks-config.sh -u=<name.surname> -t=<mfa_token>
#===============================================================================

unset AWS_ACCESS_KEY_ID
unset AWS_PROFILE
unset AWS_REGION
unset AWS_SECRET_ACCESS_KEY
unset AWS_SESSION_TOKEN

unset KUBECONFIG
unset TOKEN
unset USERNAME

REGION="eu-west-1"

KUBECONFIG_PATH=$HOME/.kube/config
SECRETS_PATH=$HOME/secrets
VARS_PATH=$SECRETS_PATH/vars.sh

help() {
  echo "Returns a set of temporary credentials for an IAM user with MFA and configures kubectl \
    so that you can connect to an Amazon EKS cluster."
  echo
  echo "Syntax: aws-eks-config.sh -u=[username] -t=[token]"
  echo
  echo "Mandatory options:"
  echo "-u | --username    IAM username."
  echo "-t | --token       MFA token code."ß
  echo
}

function create_env_file() {
    CREDENTIALS=$1
    PROFILE=$2
    REGION=$3
    echo "Exporting variables in $VARS_PATH"
    cat <<EOT > "$VARS_PATH"
export AWS_ACCESS_KEY_ID=$(echo "$CREDENTIALS" | jq -r ".Credentials.AccessKeyId")
export AWS_DEFAULT_REGION=$REGION
export AWS_PROFILE=$PROFILE
export AWS_SECRET_ACCESS_KEY=$(echo "$CREDENTIALS" | jq -r ".Credentials.SecretAccessKey")
export AWS_SESSION_TOKEN=$(echo "$CREDENTIALS" | jq -r ".Credentials.SessionToken")
export KUBECONFIG=$KUBECONFIG_PATH
EOT
}

main() {
  for i in "$@"; do
    case $i in
    -u=* | --username=*)
      USERNAME="${i#*=}"
      shift # past argument=value
      ;;
    -t=* | --token=*)
      TOKEN="${i#*=}"
      shift # past argument=value
      ;;
    -h | --help)
      help
      return
      ;;
    *)
      echo "Unknown option: ${i}, exiting..."
      return
      ;;
    esac
  done

  if [ -z ${USERNAME+x} ] && [ -z ${TOKEN+z} ]; then
    echo "USERNAME and TOKEN are unset."
    return
  elif [ -z ${USERNAME+x} ]; then
    echo "USERNAME is unset."
    return
  elif [ -z ${TOKEN+z} ]; then
    echo "TOKEN is unset."
    return
  fi

  if [[ ${#TOKEN} != 6 ]]; then
    echo "Malformed MFA TOKEN."
    return
  fi

  echo "USERNAME  = ${USERNAME}"
  echo "MFA TOKEN = ${TOKEN}"

  RESPONSE=$(aws sts get-session-token --serial-number arn:aws:iam::537084507879:mfa/"${USERNAME}" \
    --token-code "${TOKEN}")

  AWS_PROFILE="${USERNAME}"
  AWS_REGION="${REGION}"

  if [ "$(echo "${RESPONSE}" | jq -r ".Credentials.SessionToken")" ]; then
    AWS_ACCESS_KEY_ID=$(echo "${RESPONSE}" | jq -r ".Credentials.AccessKeyId")
    AWS_SECRET_ACCESS_KEY=$(echo "${RESPONSE}" | jq -r ".Credentials.SecretAccessKey")
    AWS_SESSION_TOKEN=$(echo "${RESPONSE}" | jq -r ".Credentials.SessionToken")
  else
    echo "Error in getting session token, exiting..."
    return
  fi

  # aws configure set aws_access_key_id "$AWS_ACCESS_KEY_ID" --profile "$AWS_PROFILE"
  # aws configure set aws_secret_access_key "$AWS_SECRET_ACCESS_KEY" --profile "$AWS_PROFILE"
  # aws configure set aws_session_token "$AWS_SESSION_TOKEN" --profile "$AWS_PROFILE"

  export AWS_PROFILE
  export AWS_REGION
  export AWS_ACCESS_KEY_ID
  export AWS_SECRET_ACCESS_KEY
  export AWS_SESSION_TOKEN

  create_env_file "${RESPONSE}" "${USERNAME}" "${REGION}"

  CLUSTERS=()
  while IFS=$'\n' read -r line; do CLUSTERS+=("$line"); done < <(aws eks list-clusters | jq -r '.clusters[]')
  for CLUSTER in "${CLUSTERS[@]}"; do
    aws eks update-kubeconfig --name "$CLUSTER" --alias "$CLUSTER" --region "${AWS_REGION}"
  done
}

main "$@"
