#!/usr/bin/env bash

#========================================================================================
# Title        : aws-auth.sh
# Description  : Returns a set of temporary credentials for an IAM user with MFA
#                and configures kubectl so that you can connect to an Amazon EKS cluster.
# Author       : Moneyfarm Platform Team
#========================================================================================

set -e

unset AWS_ACCESS_KEY_ID
unset AWS_REGION
unset AWS_DEFAULT_REGION
unset AWS_PROFILE
unset AWS_DEFAULT_PROFILE
unset AWS_SECRET_ACCESS_KEY
unset AWS_SESSION_TOKEN
unset KUBECONFIG

# AWS_CONFIG_FILE=~/.aws/config
AWS_DEFAULT_REGION="eu-west-1"
AWS_SESSION_DURATION_SECONDS=43200

SCRIPT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)
SCRIPT_NAME=$(basename "$0")

SECRETS_PATH="${HOME}/secrets"
K8S_INFRASTRUCTURE_PATH="${HOME}/git/moneyfarm-tech/platform/k8s-infrastructure"
VARS_PATH="${SECRETS_PATH}/vars.sh"
KUBECONFIG_PATH="${SECRETS_PATH}/kubeconfig"
mkdir -p "${SECRETS_PATH}"

function help() {
  cat <<HELP_TEXT
Usage: $0 -p <aws profile> -P <other aws profiles> -u <aws iam user> -r <aws iam role> -t <mfa token> -R <aws region> -a <aws account id> --k8s-auth

DESCRIPTION:
    Returns a set of temporary credentials for an IAM user with MFA and configures kubectl so that you can connect to an Amazon EKS cluster.

OPTIONS:
-p (required)
    AWS profile.
-u (required)
    AWS IAM user.
-t (required)
    MFA token.
--k8s-auth (optional, default is false)
    enable EKS authentication.
-P (optional)
    all the other AWS profiles to take into account.
-r (optional, default is user authentication)
    AWS IAM role.
-R, --region (optional, default is eu-west-1)
    AWS region.
-a (optional, default is moneyfarm AWS account)
    AWS account ID.
-h, --help (optional)
    print this help and exit.
HELP_TEXT
  exit 1
}

function create_env_file() {
  credentials=$1
  echo "Exporting variables in ${VARS_PATH}"
  cat <<EOT >"${VARS_PATH}"
export AWS_ACCESS_KEY_ID=$(echo "${credentials}" | jq -r ".Credentials.AccessKeyId")
export AWS_DEFAULT_REGION=$region
export AWS_PROFILE=$profile
export AWS_SECRET_ACCESS_KEY=$(echo "${credentials}" | jq -r ".Credentials.SecretAccessKey")
export AWS_SESSION_TOKEN=$(echo "${credentials}" | jq -r ".Credentials.SessionToken")
export KUBECONFIG="${KUBECONFIG_PATH}"
EOT
}

function update_kubeconfig() {
  # shellcheck source=/dev/null
  source "${VARS_PATH}"
  local -n profiles_auth_ref=$1
  for p in "${!profiles_auth_ref[@]}"; do
    if [ "${profiles_auth_ref[$p]}" = "" ]; then # IAM user or role authentication
      read -a clusters <<<"$(aws eks list-clusters | jq '.clusters[]' | xargs)"
      if [ -z "${role}" ]; then
        for cluster in "${clusters[@]}"; do
          aws eks update-kubeconfig --name "${cluster}" --kubeconfig "${KUBECONFIG_PATH}" --alias "${cluster}"
        done
      else
        for cluster in "${clusters[@]}"; do
          aws eks update-kubeconfig --name "${cluster}" --kubeconfig "${KUBECONFIG_PATH}" --alias "${cluster}"
          kubectl --kubeconfig "${KUBECONFIG_PATH}" config set-credentials "arn:aws:eks:${region}:${account_id}:cluster/${cluster}" \
            --exec-command "${SCRIPT_DIR}/${SCRIPT_NAME}" --exec-arg "get_eks_token" --exec-arg "-c ${cluster}" \
            --exec-api-version client.authentication.k8s.io/v1beta1
        done
      fi
    else # IAM assumed role authentication
      read -a clusters <<<"$(aws eks list-clusters --profile "${p}" | jq '.clusters[]' | xargs)"
      if [ -z "${role}" ]; then
        for cluster in "${clusters[@]}"; do
          aws eks update-kubeconfig --name "${cluster}" --profile "${p}" --role-arn "${profiles_auth_ref[$p]}" \
            --kubeconfig "${KUBECONFIG_PATH}" --alias "${cluster}"
        done
      else
        for cluster in "${clusters[@]}"; do
          aws eks update-kubeconfig --name "${cluster}" --profile "${p}" --role-arn "${profiles_auth_ref[$p]}" \
            --kubeconfig "${KUBECONFIG_PATH}" --alias "${cluster}"
          assumed_account_id_regex='arn:aws:iam::([0-9]+):role'
          if [[ "${profiles_auth_ref[$p]}" =~ ${assumed_account_id_regex} ]]; then
            assumed_account_id="${BASH_REMATCH[1]}"
          fi
          kubectl --kubeconfig "${KUBECONFIG_PATH}" config set-credentials "arn:aws:eks:${region}:${assumed_account_id}:cluster/${cluster}" \
            --exec-command "${SCRIPT_DIR}/${SCRIPT_NAME}" --exec-arg "get_eks_token" --exec-arg "-c ${cluster}" \
            --exec-arg "-r ${profiles_auth_ref[$p]}" --exec-api-version client.authentication.k8s.io/v1beta1
        done
      fi
    fi
  done
}

function get_eks_token() {
  # shellcheck source=/dev/null
  source "${VARS_PATH}"
  while getopts ":c:r:h" arg; do
    case $arg in
    c)
      cluster=$(echo "$OPTARG" | xargs)
      ;;
    r)
      role=$(echo "$OPTARG" | xargs)
      ;;
    h | *)
      help
      ;;
    esac
  done

  [[ -z "${cluster}" ]] && echo "Missing cluster name" && exit 1

  if [[ -z "${role}" ]]; then
    aws eks get-token --cluster-name "${cluster}"
  else
    aws eks get-token --cluster-name "${cluster}" --role-arn "${role}"
  fi
  exit 0
}

main() {
  k8s_auth_enabled=false

  # Transform long options to short ones
  for arg in "$@"; do
    shift
    case "$arg" in
    '--help') set -- "$@" '-h' ;;
    '--k8s-auth') set -- "$@" '-k' ;;
    '--region') set -- "$@" '-R' ;;
    'get_eks_token') get_eks_token "$@" ;;
    *) set -- "$@" "$arg" ;;
    esac
  done

  # Parse short options
  OPTIND=1
  while getopts ":p:u:r:t:h:R:a:P:eks" arg; do
    case "${arg}" in
    p) profile=${OPTARG} ;;
    u) username=${OPTARG} ;;
    r) role=${OPTARG} ;;
    t) token=${OPTARG} ;;
    R) region=${OPTARG} ;;
    a) account_id=${OPTARG} ;;
    P) other_profiles=${OPTARG} ;;
    k) k8s_auth_enabled=true ;;
    h | *)
      help
      ;;
    esac
  done
  shift $((OPTIND - 1))

  if [ -z "${profile}" ] || [ -z "${username}" ] || [ -z "${token}" ]; then
    help
  fi

  if [ -z "${region}" ]; then
    echo "The region is not defined, using the default eu-west-1"
    region="eu-west-1"
  fi

  if [ -z "${account_id}" ]; then
    echo "The AWS ID is not defined, using the default 537084507879"
    account_id="537084507879"
  fi

  if [ -z "${role}" ]; then
    echo "The role is not defined, authenticating into EKS using the user ${username}"
  elif [ "${role}" != "admin" ] && [ "${role}" != "developer" ] && [ "${role}" != "sighup-support" ] && [ "${role}" != "data" ]; then
    echo -e "Please, choose between the following roles:\n\tadmin\n\tdeveloper\n\tsighup-support\data"
    exit 1
  fi

  export AWS_PROFILE=$profile
  export AWS_DEFAULT_PROFILE=$profile
  export AWS_DEFAULT_REGION=$region

  if [ -z "${role}" ]; then
    credentials=$(aws sts get-session-token --serial-number arn:aws:iam::"${account_id}":mfa/"${username}" --token-code "${token}")
  else
    role_arn="arn:aws:iam::${account_id}:role/k8s-${role}-role"
    credentials=$(aws --profile "${profile}" --region "${region}" sts assume-role --role-arn "${role_arn}" --role-session-name "${username}" \
      --duration-seconds ${AWS_SESSION_DURATION_SECONDS} --token-code "${token}" --serial-number "arn:aws:iam::${account_id}:mfa/${username}")
  fi

  create_env_file "${credentials}"

  if [ "$k8s_auth_enabled" = true ]; then
    profiles=()
    profiles+=("${profile}")
    if ((${#other_profiles[@]} != 0)); then
      for p in "${other_profiles[@]}"; do
        profiles+=("${p}")
      done
    fi

    echo "[AWS profiles]"
    for p in "${profiles[@]}"; do printf "  - %s\n" "${p}"; done

    declare -A profiles_auth
    for p in "${profiles[@]}"; do
      role_arn=$(aws configure get role_arn --profile "${p}") || true
      if [[ -n "${role_arn}" ]]; then
        profiles_auth+=(["${p}"]="${role_arn}")
      else
        profiles_auth+=(["${p}"]="")
      fi
    done

    update_kubeconfig profiles_auth
    cp -rf "$KUBECONFIG_PATH" "${HOME}/.kube/config"
    if [ -d "$K8S_INFRASTRUCTURE_PATH" ]; then
      cp -rf "$KUBECONFIG_PATH" "${K8S_INFRASTRUCTURE_PATH}/secrets"
      cp -rf "$VARS_PATH" "${K8S_INFRASTRUCTURE_PATH}/secrets"
    fi
  fi

  # shellcheck source=/dev/null
  . "${VARS_PATH}"
}

main "$@"
