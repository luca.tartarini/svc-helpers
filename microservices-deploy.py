import os
import subprocess
import time

from git import Repo
from dotenv import load_dotenv

# Pre-requisites:
# - clone all the needed git projects in a folder and specify the path
# - mcl installed
# - kubectl installed
# - authenticated to EKS contexts
# - Vault environment variables loaded

context = "stage"
skip_render = False

dir_list = []
render_processes = []

MICROSERVICES_DIRECTORY = "/Users/luca.tartarini/git/Moneyfarm-Tech/microservices"
VAULT_FILE = "/Users/luca.tartarini/.{}_vault".format(context)

CONTEXT_COMMAND = "kubectx {}".format(context)
RENDER_COMMAND = "mcl pipeline run-step render-{}-k8s-stack".format(context)
DIFF_COMMAND = "kubectl diff -f .{}-k8s.yaml --context={}".format(
    context, context)
DEPLOY_COMMAND = "mcl pipeline run-step deploy-on-{}-k8s".format(context)

load_dotenv(VAULT_FILE)
print("INFO: Vault address: {}".format(os.environ.get("VAULT_ADDR")))
print("INFO: Vault token: {}".format(os.environ.get("VAULT_TOKEN")))

e = dict(os.environ)

context_process = subprocess.Popen(
    CONTEXT_COMMAND.split(), stdout=subprocess.PIPE, env=e, universal_newlines=True)
context_process.wait()
output, error = context_process.communicate()
print(output)

# Populate directory list
for f in os.listdir(MICROSERVICES_DIRECTORY):
    d = os.path.join(MICROSERVICES_DIRECTORY, f)
    if os.path.isdir(d):
        repo = Repo(d)
        for ref in repo.references:
            if 'master' == ref.name:
                branch_name = 'master'
                break
            elif 'main' == ref.name:
                branch_name = 'main'
                break
        repo.git.checkout(branch_name)
        o = repo.remotes.origin
        print("INFO: service: {}, directory: {}, pulling from {} branch.".format(
            d.rsplit('/', 1)[1], d, branch_name))
        try:
            o.pull()
        except:
            print("ERROR: an exception occurred in ", branch_name, " branch.")
        else:
            dir_list += [d]

dir_list.sort()

# Render microservices templates
if skip_render == False:
    counter = 0
    print("INFO: starting rendering processes...")
    for d in dir_list:
        counter += 1
        print(
            "INFO: [{}/{}] - rendering {}...".format(counter, len(dir_list), d.rsplit('/', 1)[1]))
        render_process = subprocess.Popen(
            RENDER_COMMAND.split(), stdout=subprocess.DEVNULL,
            stderr=subprocess.STDOUT, cwd=d, env=e, universal_newlines=True)
        render_processes.append(render_process)
        time.sleep(5)
    for p in render_processes:
        p.wait()
    print("INFO: rendering processes finished.")

# Diff and conditional deployment
for d in dir_list:
    service = d.rsplit('/', 1)[1]
    diff_process = subprocess.Popen(
        DIFF_COMMAND.split(), stdout=subprocess.PIPE, cwd=d, env=e, universal_newlines=True)
    diff_process.wait()
    output, error = diff_process.communicate()
    print(output)
    while True:
        answer = input(
            "[?] Do you want to deploy {} in {} environment? (yes/no) ".format(service, context))
        while answer.lower() not in ("yes", "no"):
            answer = input(
                "[?] Do you want to deploy {} in {} environment? (yes/no) ".format(service, context))
        if answer == "yes":
            print("INFO: deploying {} service in {} environment.".format(
                service, context))
            deploy_process = subprocess.Popen(
                DEPLOY_COMMAND.split(), stdout=subprocess.PIPE, cwd=d, env=e, universal_newlines=True)
            deploy_process.wait()
            output, error = deploy_process.communicate()
            print(output)
            break
        else:
            print("INFO: skipping {} deployment in {} environment.".format(
                service, context))
            break
